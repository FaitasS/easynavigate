@echo off

set MARKPATH=%HOMEPATH%
set MARKFILE=%MARKPATH%\.marks
set MARKFILECOPY=%MARKPATH%\.marks2

if [%1] == [] call:usage
if %1 == usage call:usage
if %1 == mark call:mark %2 %3
if %1 == unmark call:unmark %2
if %1 == jump call:jump %2
if %1 == marks call:marks
if %1 == clear call:clear
GOTO:EOF

:usage
	echo List of commands:
	echo mark [name[, path]] - save path for the future (if path is set to 'this', it uses current path)
	echo unmark [name] - delete mark from marks file
	echo jump [name] - go to path using mark's name
	echo marks - list of all marks
	echo clear - delete all marks
GOTO:EOF

:mark
	if %2 == this set THISPATH=%CD%
	if not %2 == this set THISPATH=%2
	echo %1 %THISPATH% >>%MARKFILE%
GOTO:EOF

:unmark
	type %MARKFILE% | findstr /B /V %1 > %MARKFILECOPY%
	del %MARKFILE%
	move /Y %MARKFILECOPY% %MARKFILE% >nul
GOTO:EOF

:jump
	for /F "tokens=*" %%i IN (%MARKFILE%) DO (
		for /F "tokens=1" %%j IN ("%%i") DO (
			if %%j == %1 (
				for /F "tokens=2" %%k IN ("%%i") DO cd %%k
			)
		)
	)
GOTO:EOF

:marks
	for /F "tokens=*" %%i IN (%MARKFILE%) DO echo %%i
GOTO:EOF

:clear
	type NUL >%MARKFILE%
GOTO:EOF