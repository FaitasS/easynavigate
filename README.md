# EasyNavigate

`EasyNavigate` is a Batch Script for easy navigating your Windows OS filesystem in CMD.

### Why to use `EasyNavigate`?
When you usually use a CMD, you just type the paths (`C:\Users\User\Desktop\something\deeper\goes\here`)
all the time you want to go there. And it annoys, doesn't it? Especially when you are developing
something and you use CMD to go from one path, run one script, go to another, and run another script.
And you repeat these actions with the same paths several times per day. There `EasyNavigate` helps you.

### What can `EasyNavigate` do?
* Mark/Unmark any path for future use
* Jump to any path which is provided in `.marks` file
* Clear all marks

### How to start using `EasyNavigate`?
* Put `navigate.bat` file in any place you like
* Go to __Control Panel -> System -> Advanced System Settings -> Environment Variables -> Select `PATH`
and click `Edit` -> Add your `navigate.bat` path to `Variable value`. Don't forget to seperate it with semicolon (;)__
* And you are good to go. Just go to `CMD.exe` and type `navigate`.
